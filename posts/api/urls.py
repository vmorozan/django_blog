from django.conf.urls import url
from django.contrib import admin

from .views import (
    PostListAPIView,
    PostDetailAPIView,
    PostDeleteAPIView,
    PostUpdateAPIView,
    PostCreateAPIView,
)

urlpatterns = [
    url(r'^create/$', PostCreateAPIView.as_view(), name='create'),
    url(r'^$', PostListAPIView.as_view(), name='list_api'),
    url(r'^(?P<pk>\d+)/$', PostDetailAPIView.as_view(), name='detail_api'),
    url(r'^(?P<pk>\d+)/edit/$', PostUpdateAPIView.as_view(), name='update_api'),
    url(r'^(?P<pk>\d+)/delete/$', PostDeleteAPIView.as_view(), name='delete_api'),

]
